#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <opencv2/opencv.hpp>
#include <QIcon>

using namespace cv;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    gaussianKernel = 3;
    brightness = 50;
    contrast = 50;
    scaleFactor = 1.0;
    trackbar = false;
    angle  = 0;
    isRecording = false;

    setFilter(FFilters::None);
    setOperation(FOperations::Nope);

    ui->setupUi(this);

    createActions();
    createToolBar();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::capture()
{
    int camera = 0;
    //VideoCapture cap;
    char option, read;
    Mat frame;

    //bool trackbar = false;
    // open the default camera, use something different from 0 otherwise;
    // Check VideoCapture documentation.
    if(!cap.open(camera))
        exit(0);
    for(;;)
    {
        cap >> frame;
        if( frame.empty() ) break; // end of video stream

        if((read = waitKey(1000/30)) == 27 ) break; // stop capturing by pressing ESC

        if(read != -1)
        {
            option = read;
        }

        switch(read)
        {
            case 'g':
            case 'G':
                setFilter(FFilters::GaussianFilter);
                break;
            case 'c':
            case 'C':
                setFilter(FFilters::CannyFilter);
                break;
            case 's':
            case 'S':
                setFilter(FFilters::SobelFilter);
                break;
            case 'n':
            case 'N':
                setFilter(FFilters::NegativeFilter);
                break;
            case 'r':
            case 'R':
                setFilter(FFilters::GrayScaleFilter);
                break;
            case '+':
                setOperation(FOperations::ZoomInOp);
                break;
            case '-':
                setOperation(FOperations::ZoomOutOp);
                break;
            case '>':
                setOperation(FOperations::RotateRight);
                break;
            case '<':
                setOperation(FOperations::RotateLeft);
                break;
            case 'v':
            case 'V':
                if(getOperationOption() != FOperations::MirrorVertically)
                    setOperation(FOperations::MirrorVertically);
                else setOperation(FOperations::Nope);
                break;
            case 'h':
            case 'H':
                if(getOperationOption() != FOperations::MirrorHorizontally)
                    setOperation(FOperations::MirrorHorizontally);
                else setOperation(FOperations::Nope);
                break;
            case 'w':
            case 'W':
            if(!isRecording)
            {
                isRecording = true;
                qDebug("CV_CAP_PROP_FRAME_HEIGHT %i", (int)cap.get(CV_CAP_PROP_FRAME_HEIGHT));
                /*vwriter.open("video.avi", CV_FOURCC('M', 'J', 'P', 'G'), cap.get(CV_CAP_PROP_FPS),
                             Size((int)cap.get(CV_CAP_PROP_FRAME_HEIGHT),
                                  (int)cap.get(CV_CAP_PROP_FRAME_WIDTH)), true);
                */
                double fps = cap.get(CV_CAP_PROP_FPS);
                //double fps = 10.0;
                vwriter.open("video.avi", CV_FOURCC('M', 'J', 'P', 'G'), fps/3,
                             Size((int)cap.get(CV_CAP_PROP_FRAME_WIDTH),
                                  (int)cap.get(CV_CAP_PROP_FRAME_HEIGHT)), true);
                if(!vwriter.isOpened())
                    qDebug("Coud not open video writer.");
            }
            else
            {
                isRecording = false;
                vwriter.release();
            }
                option = 0;
                break;
            default:
                break;
        }

        displayOriginal(frame);
        displayFiltered(frame);
    }
    cap.release();
    vwriter.release();
    destroyAllWindows();
}

Mat MainWindow::applyGaussian(Mat f)
{
    if((gaussianKernel % 2) == 0)
        gaussianKernel++;
    GaussianBlur(f, f, Size(gaussianKernel,gaussianKernel), 0, 0);
    return f;
}

Mat MainWindow::applyCanny(Mat f)
{
    cvtColor(f, f, CV_BGR2GRAY);    
    Canny(f, f, 0, 30, 3);
    cvtColor(f, f, CV_GRAY2BGR );
    return f;
}

Mat MainWindow::applySobel(Mat f)
{
    Sobel(f, f, -1, 2, 2, 5);
    return f;
}

Mat MainWindow::setContrast(Mat f)
{
    f.convertTo(f, -1, contrast/50.0, 0);
    return f;
}

Mat MainWindow::setBrightness(Mat f)
{
    f.convertTo(f, -1, 1, brightness - 50);
    return f;
}

Mat MainWindow::negative(Mat f)
{
    f.convertTo(f, -1, -1, 255);
    return f;
}

Mat MainWindow::grayScale(Mat f)
{
    cvtColor(f, f, CV_BGR2GRAY );
    cvtColor(f, f, CV_GRAY2BGR );
    return f;
}

void MainWindow::zoomIn()
{
    scaleFactor += 0.25;
    if(scaleFactor >= 2.0)
        scaleFactor = 2.0;
}

void MainWindow::zoomOut()
{
    scaleFactor -= 0.25;
    if(scaleFactor <= 0.25)
        scaleFactor = 0.25;
}

Mat MainWindow::scale(Mat f)
{
    if(scaleFactor > 1.0)
    {
        cv::resize(f,f, Size(), scaleFactor, scaleFactor, CV_INTER_CUBIC);
    }
    else
    {
        cv::resize(f,f, Size(), scaleFactor, scaleFactor, CV_INTER_AREA);
    }

    f = rotate(f);

    return f;
}

void MainWindow::setFilter(FFilters f)
{
    filter = f;
}

FFilters MainWindow::getFilterOption()
{
    return filter;
}

void MainWindow::setOperation(FOperations o)
{
    operation = o;
}

FOperations MainWindow::getOperationOption()
{
    return operation;
}

void MainWindow::displayOriginal(Mat fr)
{
    imshow("Original", fr);
}

void MainWindow::displayFiltered(Mat fr)
{
    Mat filtered;
    filtered = fr;
    filtered = setBrightness(filtered);
    filtered = setContrast(filtered);

    switch (getFilterOption()) {
    case FFilters::GaussianFilter:
        filtered = applyGaussian(filtered);
        break;
    case FFilters::CannyFilter:
        filtered = applyCanny(filtered);
        break;
    case FFilters::SobelFilter:
        filtered = applySobel(filtered);
        break;
    case FFilters::NegativeFilter:
        filtered = negative(filtered);
        break;
    case FFilters::GrayScaleFilter:
        filtered = grayScale(filtered);
        break;
    default:
        break;
    }

    switch (getOperationOption())
    {
    case FOperations::ZoomInOp:
        zoomIn();
        setOperation(FOperations::Nope);
        break;
    case FOperations::ZoomOutOp:
        zoomOut();
        setOperation(FOperations::Nope);
        break;
    case FOperations::RotateLeft:
        setAngle(90);
        setOperation(FOperations::Nope);
        break;
    case FOperations::RotateRight:
        setAngle(-90);
        setOperation(FOperations::Nope);
        break;
    case FOperations::MirrorVertically:
        filtered = mirrorVerfically(filtered);
        break;
    case FOperations::MirrorHorizontally:
        filtered = mirrorHorizontally(filtered);
        break;
    default:
        break;
    }

    cvCreateTrackbar("Guassian Kernel Size", "Filtered", &gaussianKernel, 100, NULL);
    cvCreateTrackbar("Brightness", "Filtered", &brightness, 100, NULL);
    cvCreateTrackbar("Contrast", "Filtered", &contrast, 100, NULL);
    trackbar = true;

    filtered = scale(filtered);

    imshow("Filtered", filtered);


    if(vwriter.isOpened() && isRecording)
    {
        qDebug("Recording...");
        vwriter.write(filtered);
    }
}

Mat MainWindow::rotate(Mat f)
{
    Mat t = f;
    Mat fliped;

    switch(getAngle())
    {
        case 0:
            fliped = f;
            break;
        case 90:
            cv::transpose(f, fliped);
            break;
        case 180:
            cv::flip(t, fliped, 0);
            break;
        case 270:
            cv::transpose(f, t);
            cv::flip(t, fliped, 1);
            break;
        default:
            break;
    }

    return fliped;
}

void MainWindow::setAngle(int a)
{
    angle = angle + a;

    if(angle >= 360)
        angle = 0;
    else if(angle < 0)
        angle = 270;

    qDebug("Angle: %i", angle);
}

int MainWindow::getAngle()
{
    return angle;
}

Mat MainWindow::mirrorVerfically(Mat f)
{
    Mat flipped;
    cv::flip(f, flipped, 1);
    return flipped;
}

Mat MainWindow::mirrorHorizontally(Mat f)
{
    Mat flipped;
    cv::flip(f, flipped, 0);
    return flipped;
}

void MainWindow::createActions()
{
    gaussianBlurAct = new QAction(QIcon::fromTheme("Gaussian"), tr("Gaussian Blur"), this);
    gaussianBlurAct->setStatusTip(tr("Gaussian Blur"));
    connect(gaussianBlurAct, &QAction::triggered, this, &MainWindow::setGaussianBlur);

    cannyAct = new QAction(QIcon::fromTheme("Canny"), tr("Canny"), this);
    cannyAct->setStatusTip(tr("Canny"));
    connect(cannyAct, &QAction::triggered, this, &MainWindow::setCanny);

    sobelAct = new QAction(QIcon::fromTheme("Sobel"), tr("Sobel"), this);
    sobelAct->setStatusTip(tr("Sobel"));
    connect(sobelAct, &QAction::triggered, this, &MainWindow::setSobel);

    luminanceAct = new QAction(QIcon::fromTheme("image-x-generic"), tr("Luminance"), this);
    luminanceAct->setStatusTip(tr("Luminance"));
    connect(luminanceAct, &QAction::triggered, this, &MainWindow::setLuminace);

    negativeAct = new QAction(QIcon::fromTheme("image-missing"), tr("Negative"), this);
    negativeAct->setStatusTip(tr("Negative"));
    connect(negativeAct, &QAction::triggered, this, &MainWindow::setNegative);

    zoomInAct = new QAction(QIcon::fromTheme("zoom-in"), tr("Zoom in"), this);
    zoomInAct->setStatusTip(tr("Zoom in"));
    connect(zoomInAct, &QAction::triggered, this, &MainWindow::doZoomIn);

    zoomOuAct = new QAction(QIcon::fromTheme("zoom-out"), tr("Zoom out"), this);
    zoomOuAct->setStatusTip(tr("Zoom out"));
    connect(zoomOuAct, &QAction::triggered, this, &MainWindow::doZoomOut);

    rotateLeftAct = new QAction(QIcon::fromTheme("object-rotate-left"), tr("Rotate left"), this);
    rotateLeftAct->setStatusTip(tr("Rotate left"));
    connect(rotateLeftAct, &QAction::triggered, this, &MainWindow::doRotateLeft);

    rotateRightAct = new QAction(QIcon::fromTheme("object-rotate-right"), tr("Rotate right"), this);
    rotateRightAct->setStatusTip(tr("Rotate right"));
    connect(rotateRightAct, &QAction::triggered, this, &MainWindow::doRotateLeft);

    verticalMirrorAct = new QAction(QIcon::fromTheme("object-flip-horizontal"), tr("Vertical mirror"), this);
    verticalMirrorAct->setStatusTip(tr("Vertical mirror"));
    connect(verticalMirrorAct, &QAction::triggered, this, &MainWindow::doVerticallMirror);

    horizontallMirrorAct = new QAction(QIcon::fromTheme("object-flip-vertical"), tr("Horizontal mirror"), this);
    horizontallMirrorAct->setStatusTip(tr("Horizontal mirror"));
    connect(horizontallMirrorAct, &QAction::triggered, this, &MainWindow::doHorizontallMirror);

    recordAct = new QAction(QIcon::fromTheme("media-record"), tr("Record"), this);
    recordAct->setStatusTip(tr("Record"));
    connect(recordAct, &QAction::triggered, this, &MainWindow::doRecord);
}

void MainWindow::createToolBar()
{
    ui->mainToolBar->addAction(gaussianBlurAct);
    ui->mainToolBar->addAction(cannyAct);
    ui->mainToolBar->addAction(sobelAct);
    ui->mainToolBar->addAction(luminanceAct);
    ui->mainToolBar->addAction(negativeAct);
    ui->mainToolBar->addAction(zoomInAct);
    ui->mainToolBar->addAction(zoomOuAct);
    ui->mainToolBar->addAction(rotateLeftAct);
    ui->mainToolBar->addAction(rotateRightAct);
    ui->mainToolBar->addAction(verticalMirrorAct);
    ui->mainToolBar->addAction(horizontallMirrorAct);
    ui->mainToolBar->addAction(recordAct);
}

void MainWindow::setGaussianBlur()
{
    setFilter(FFilters::GaussianFilter);
}

void MainWindow::setCanny()
{
    setFilter(FFilters::CannyFilter);
}

void MainWindow::setSobel()
{
    setFilter(FFilters::SobelFilter);
}

void MainWindow::setLuminace()
{
    setFilter(FFilters::GrayScaleFilter);
}

void MainWindow::setNegative()
{
    setFilter(FFilters::NegativeFilter);
}

void MainWindow::doZoomIn()
{
    setOperation(FOperations::ZoomInOp);
}

void MainWindow::doZoomOut()
{
    setOperation(FOperations::ZoomOutOp);
}

void MainWindow::doRotateLeft()
{
    setOperation(FOperations::RotateLeft);
}

void MainWindow::doRotateRight()
{
    setOperation(FOperations::RotateRight);
}

void MainWindow::doVerticallMirror()
{
    if(getOperationOption() != FOperations::MirrorVertically)
        setOperation(FOperations::MirrorVertically);
    else setOperation(FOperations::Nope);
}

void MainWindow::doHorizontallMirror()
{
    if(getOperationOption() != FOperations::MirrorHorizontally)
        setOperation(FOperations::MirrorHorizontally);
    else setOperation(FOperations::Nope);
}

void MainWindow::doRecord()
{
    if(!isRecording)
    {
        isRecording = true;
        qDebug("CV_CAP_PROP_FRAME_HEIGHT %i", (int)cap.get(CV_CAP_PROP_FRAME_HEIGHT));
        /*vwriter.open("video.avi", CV_FOURCC('M', 'J', 'P', 'G'), cap.get(CV_CAP_PROP_FPS),
                     Size((int)cap.get(CV_CAP_PROP_FRAME_HEIGHT),
                          (int)cap.get(CV_CAP_PROP_FRAME_WIDTH)), true);
        */
        double fps = cap.get(CV_CAP_PROP_FPS);
        //double fps = 10.0;
        vwriter.open("video.avi", CV_FOURCC('M', 'J', 'P', 'G'), fps/3,
                     Size((int)cap.get(CV_CAP_PROP_FRAME_WIDTH),
                          (int)cap.get(CV_CAP_PROP_FRAME_HEIGHT)), true);
        if(!vwriter.isOpened())
            qDebug("Coud not open video writer.");
    }
    else
    {
        isRecording = false;
        vwriter.release();
    }
}
