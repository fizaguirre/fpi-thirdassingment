#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <opencv2/opencv.hpp>

using namespace cv;

enum FFilters
{
    GaussianFilter,
    CannyFilter,
    SobelFilter,
    NegativeFilter,
    GrayScaleFilter,
    None
};

enum FOperations
{
    ZoomInOp,
    ZoomOutOp,
    RotateLeft,
    RotateRight,
    MirrorVertically,
    MirrorHorizontally,
    Nope
};

enum FOrientation
{
    Left,
    Right
};

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void capture();
    Mat applyGaussian(Mat f);
    Mat applyCanny(Mat f);
    Mat applySobel(Mat f);
    Mat setContrast(Mat f);
    Mat setBrightness(Mat f);
    Mat negative(Mat f);
    Mat grayScale(Mat f);
    void zoomIn();
    void zoomOut();
    Mat scale(Mat f);

    void setFilter(FFilters f);
    FFilters getFilterOption();
    void setOperation(FOperations o);
    FOperations getOperationOption();

    void displayOriginal(Mat fr);
    void displayFiltered(Mat fr);

    Mat rotate(Mat f);
    void setAngle(int a);
    int getAngle();

    Mat mirrorVerfically(Mat f);
    Mat mirrorHorizontally(Mat f);

    void createActions();
    void createToolBar();

    void setGaussianBlur();
    void setCanny();
    void setSobel();
    void setLuminace();
    void setNegative();
    void doZoomIn();
    void doZoomOut();
    void doRotateLeft();
    void doRotateRight();
    void doVerticallMirror();
    void doHorizontallMirror();
    void doRecord();

private:
    Ui::MainWindow *ui;
    int gaussianKernel;
    int brightness, contrast;
    float scaleFactor;
    bool trackbar;
    int angle;
    bool isRecording;

    VideoWriter vwriter;

    FFilters filter;
    FOperations operation;

    VideoCapture cap;


    QAction* gaussianBlurAct;
    QAction* cannyAct;
    QAction* sobelAct;
    QAction* luminanceAct;
    QAction* zoomInAct;
    QAction* zoomOuAct;
    QAction* rotateLeftAct;
    QAction* rotateRightAct;
    QAction* verticalMirrorAct;
    QAction* horizontallMirrorAct;
    QAction* negativeAct;
    QAction* recordAct;

};

#endif // MAINWINDOW_H
